﻿using MUA.Markup.UnitTests.String_Operations.DataSource;
using Xunit;

namespace MUA.Markup.UnitTests.String_Operations
{
    public class StringTests
    {
        [Theory]
        [MemberData(nameof(StringTestFactory.Constructor), MemberType = typeof(StringTestFactory))]
        public void StringConstructor(MarkupString constructed, string expected)
        {
            Assert.Equal(expected, constructed.ToString());
        }

        [Theory]
        [MemberData(nameof(StringTestFactory.Insert), MemberType = typeof(StringTestFactory))]
        public void Insert(MarkupString original, MarkupString adding, int index, string expected)
        {
            MarkupString inserted = original.Insert(adding, index);
            Assert.Equal(expected, inserted.ToString());
        }

        [Theory]
        [MemberData(nameof(StringTestFactory.BasicCopy), MemberType = typeof(StringTestFactory))]
        public void Copy(MarkupString origin)
        {
            MarkupString copy = new MarkupString();
            origin.CopyInto(copy);
            Assert.Equal(origin.ToString(), copy.ToString());
        }

        [Theory]
        [MemberData(nameof(StringTestFactory.Remove), MemberType = typeof(StringTestFactory))]
        public void Remove(MarkupString original, int startIndex, int count, string expected)
        {
            original.Remove(startIndex, count);
            Assert.Equal(expected, original.ToString());
        }
    }
}
