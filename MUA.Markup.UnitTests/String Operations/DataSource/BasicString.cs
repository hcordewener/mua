﻿using System.Collections.Generic;

namespace MUA.Markup.UnitTests.String_Operations.DataSource
{
    public class StringTestFactory
    {
        public static IEnumerable<object[]> Remove
        {
            get
            {
                yield return new object[] { new MarkupString("123456"), 0, 1, "23456" };
                yield return new object[] { new MarkupString("123456"), 5, 1, "12345" };
                yield return new object[] { new MarkupString("123456"), 2, 1, "12456" };
                yield return new object[] { new MarkupString("123456"), 0, 0, "123456" };
                yield return new object[] { new MarkupString("123456"), 5, 0, "123456" };
                yield return new object[] { new MarkupString("123456"), 2, 0, "123456" };
            }
        }

        public static IEnumerable<object[]> BasicCopy
        {
            get
            {
                yield return new object[] { new MarkupString("This is a test string") };
            }
        }

        public static IEnumerable<object[]> Insert
        {
            get
            {
                yield return new object[] { new MarkupString("left"), new MarkupString("right"), 4, "leftright" };
                yield return new object[] { new MarkupString("left"), new MarkupString("right"), 0, "rightleft" };
                yield return new object[] { new MarkupString("xxxx"), new MarkupString("center"), 2, "xxcenterxx" };
            }
        }

        public static IEnumerable<object[]> Constructor
        {
            get
            {
                yield return new object[] { new MarkupString("This is a test string"), "This is a test string" };
            }
        }
    }
}
