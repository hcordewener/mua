﻿using System.Collections;
using System.Collections.Generic;

namespace MUA.Markup.UnitTests.String_Operations.DataSource
{
    public class CompoundString
    {
        public static IEnumerable<object[]> Remove
        {
            get
            {
                MarkupString BaseString() => new MarkupString("1256");
                MarkupString CompoundString() => new MarkupString(BaseString()).Insert("34", 2);
                yield return new object[] { CompoundString(), 0, 1, "23456" };
                yield return new object[] { CompoundString(), 5, 1, "12345" };
                yield return new object[] { CompoundString(), 2, 1, "12456" };
                yield return new object[] { CompoundString(), 0, 0, "123456" };
                yield return new object[] { CompoundString(), 5, 0, "123456" };
                yield return new object[] { CompoundString(), 2, 0, "123456" };
            }
        }

        public static IEnumerable<object[]> BasicCopy
        {
            get
            {
                MarkupString BaseString() => new MarkupString("This is a ");
                MarkupString CompoundString() => new MarkupString(BaseString().Append(new MarkupString("test string")));
                yield return new object[] { CompoundString() };
            }
        }

        public static IEnumerable<object[]> Insert
        {
            get
            {
                MarkupString Left() => new MarkupString("le");
                MarkupString CompoundLeft() => new MarkupString(Left().Append(new MarkupString("ft")));
                MarkupString Right() => new MarkupString("ri");
                MarkupString CompoundRight() => new MarkupString(Right().Append(new MarkupString("ght")));
                MarkupString Xxx() => new MarkupString("xx");
                MarkupString Compoundxxx() => new MarkupString(Xxx().Append(new MarkupString("xx")));
                MarkupString Center() => new MarkupString("cen");
                MarkupString CompoundCenter() => new MarkupString(Center().Append(new MarkupString("ter")));
                yield return new object[] { CompoundLeft(), CompoundRight(), 4, "leftright" };
                yield return new object[] { CompoundLeft(), CompoundRight(), 0, "rightleft" };
                yield return new object[] { Compoundxxx(), CompoundCenter(), 2, "xxcenterxx" };
            }
        }

        public static IEnumerable<object[]> Constructor
        {
            get
            {
                MarkupString BaseString() => new MarkupString("This is a ");
                MarkupString CompoundString() => new MarkupString(BaseString().Append(new MarkupString("test string")));
                yield return new object[] { CompoundString(), "This is a test string" };
            }
        }
    }
}
