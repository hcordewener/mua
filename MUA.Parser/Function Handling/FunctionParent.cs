﻿namespace MUA.Parser
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    ///     The FunctionParent is intended by be inherited by all functions. A call back to ExecuteBody,
    ///     only defined in the parent, will then execute a function's body.
    /// </summary>
    public class FunctionParent : IFunction
    {
        public string Name { get; set; }
    
        public uint Flags { get; set; }

        public string Type { get; set; }

        public Permission Permissions { get; set; }

        public FunctionResult ExecuteBody(object obj, Stack<FunctionResult> arguments)
        {
            this.Permissions.HasPermission(obj);
            throw new NotImplementedException();
        }
    }
}