﻿using System.Collections.Generic;

namespace MUA.Parser
{
    /// <summary>
    ///     This interface ensures that a new function definition has all of the following items defined.
    /// </summary>
    public interface IFunction
    {
        /// <summary>
        ///     Gets or sets the name of the function.
        /// </summary>
        string Name { get; set; }

        /// <summary>
        ///     Gets or sets the flags that control how the function is parsed.
        /// </summary>
        /// <remarks>
        ///     Type may change.
        /// </remarks>
        uint Flags { get; set; }

        /// <summary>
        ///     Gets or sets the Type - whether it is Soft- or Hard-Code
        /// </summary>
        string Type { get; set; }

        /// <summary>
        ///     Gets or sets the Permissions of the function.
        /// </summary>
        Permission Permissions { get; set; }

        /// <summary>
        ///     Executes the body of the child. This will never be called on itself.
        /// </summary>
        /// <param name="obj">The object executing the body, used for the sake of evaluating Permissions.</param>
        /// <param name="arguments">A Stack of FunctionResults that represent the arguments.</param>
        /// <returns>The MarkupString to be appended as a result of this function call.</returns>
        /// <remarks>This may have to be changed to a FunctionResultStringTuple or something similar to enable the use of tuples.</remarks>
        FunctionResult ExecuteBody(object obj, Stack<FunctionResult> arguments);

    }
}