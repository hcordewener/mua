﻿using System;
using System.IO;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.Json;


namespace MUA.Configuration
{
    public static class ConfigurationSettings
    {
        public static int ClientBufferLength => int.Parse(Configuration.Instance["Client:Buffer Length"]);

        public static Uri TelnetAddress
        {
            get
            {
                var uriBuilder = new UriBuilder
                {
                    Host = Configuration.Instance["Server:Local Address"],
                    Port = int.Parse(Configuration.Instance["Server:Telnet Port"]),
                    Scheme = Configuration.Instance["Server:Scheme"]
                };
                return uriBuilder.Uri;
            }
        }
    }

    internal class Configuration
    {
        public IConfigurationRoot Default { get; set; }

        private static Configuration _instance;
        public static IConfigurationRoot Instance => _instance?.Default ?? (_instance = new Configuration()).Default;

        private Configuration()
        {
            var builder = new ConfigurationBuilder()
                 .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

            Default = builder.Build();
        }
    }
}
