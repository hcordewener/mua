﻿using MUA.Markup;
using MUA.Server.TCP;
using Xunit;

namespace MUA.Server.NUnit.Integration
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    ///     The Main Program.
    /// </summary>
    public class MainProgram
    {

        /// <summary>
        /// StringTest tests strings. To be converted into a UnitTest later.
        /// </summary>
        public static void StringTest()
        {
            Console.Read();

            const MarkupRule hiLite = MarkupRule.HiLight;
            var hiLiteAsList = new HashSet<MarkupRule> { hiLite };
            var testString1 = new MarkupString(new Markup.Markup(hiLiteAsList));
            var testString2 = new MarkupString();

            /* ----------------------------------
             *  Testing Ansi Insert and Remove
             * ---------------------------------- */
            Console.WriteLine("Inserting DOOD into HiLite markup string 1 at position 0.");
            testString1.Insert("DOOD", 0);
            Console.WriteLine("Inserting Omnomnom into markup string 2 at position 0.");
            testString2.Insert("Omnomnom", 0);
            Console.WriteLine("Inserting markup string 1 into markup string 2 at position 4.");
            testString2.Insert(testString1, 4);
            Console.WriteLine("Printing markup string 2: ");
            Console.WriteLine(testString2.ToString());
            Console.WriteLine("Removing 4 characters starting at position 3 from markup string 2.");
            testString2.Remove(3, 4);
            Console.WriteLine("Printing markup string 2: ");
            Console.WriteLine(testString2.ToString());

            /* ---------------------------------- 
             *  Testing Ansi Flattening
             * ---------------------------------- */
            Console.WriteLine("Ansi Flattening Tests");
            var testString3 = new List<MarkupString>();
            testString2.FlattenInto(testString3);

            Console.WriteLine("Flattening string 2 into string 3, and printing.");
            Console.WriteLine(testString2.ToString());

            var sb2 = new StringBuilder();
            foreach (MarkupString each in testString3)
            {
                sb2.Append(each.ToTestString());
            }

            Console.WriteLine(sb2.ToString());
            Console.WriteLine("\n\n\n");
            Console.ReadLine();


            Console.WriteLine("Creating string 4 from string 2 (" + testString2 + "), starting at position 2, length 4.");
            var testString4 = new MarkupString(testString2, 2, 4);
            Console.WriteLine(testString4.ToString());

            Console.WriteLine("\nInserting 'Graaaa' into string 4 at position 2.");
            testString4.Insert("Graaaa", 2);

            Console.WriteLine("Printing test string 4");
            Console.WriteLine(testString4);

            Console.WriteLine("Replacing string 4 at position 3 for length 4 with 'IttyBittyKittyCommitty'");
            testString4.Replace(new MarkupString("IttyBittyKittyCommitty"), 3, 4);
            Console.WriteLine("Printing test string 4");
            Console.WriteLine(testString4);

            Console.WriteLine("Replacing string 4 at position 4 for length 2 with string 2 (" + testString2 + ")");
            testString4.Replace(testString2, 4, 2);
            Console.WriteLine("Printing test string 4");
            Console.WriteLine(testString4);
            Console.ReadLine();
        }

        [Fact(Skip = "Does not exit. Need to move to OWIN.")]
        public static void ServeItUp()
        {
            Console.WriteLine("Rawr");
            TCPInitializer tcp = new TCPInitializer();
            tcp.Serve();
            Console.ReadLine();
            Assert.True(true);
        }
    }
}