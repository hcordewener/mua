﻿using Xunit;

namespace MUA.Parser.UnitTests.Basic
{
    class BasicFunctionEvaluation
    {
        [Fact]
        public void Test(string evaluate, string expected)
        {
            // p used for testUnit.
            var p = new MUA.Parser.Parser();
            var result = p.FunctionParse(evaluate);
            Assert.Equal(expected, result.ToString());
        }
    }
}
