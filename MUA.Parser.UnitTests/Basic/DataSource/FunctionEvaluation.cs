﻿using System.Collections;

namespace MUA.Parser.UnitTests.Basic.DataSource
{
    public class FunctionEvaluation
    {
        public static IEnumerable Basic
        {
            get
            {
                yield return new object[] { @"fun(abc) fun3()", "This was once function fun! fun3()" };
                yield return new object[] { @"fun(abc) [fun3()]", "This was once function fun! This was once function fun3!" };
                yield return new object[] { @"fun(\\\)\)) fun3()", "This was once function fun! fun3()" };
                yield return new object[] { @"fun(fun2(\)\\\(( fun3())", "This was once function fun!" };
                yield return new object[] { @"fun(fun2(() fun3()))", "This was once function fun!" };
                yield return new object[] { @"fun(fun2(() fun3()))\", "This was once function fun!" };
            }
        }
    }
}